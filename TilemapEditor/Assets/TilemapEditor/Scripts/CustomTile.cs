/****************************************************
    File Name  : CustomTile.cs
    Author     : MingJia Li
    Date & Ver : 2021/11/28 - Unity2021.1.19f1
    Description:
*****************************************************/

using UnityEngine;

[CreateAssetMenu(fileName = "CustomTile", menuName = "Custom Tile")]
public class CustomTile : ScriptableObject
{
    [SerializeField]
    public Sprite SourceSprite;

    [SerializeField]
    public Color SpriteColor;
}