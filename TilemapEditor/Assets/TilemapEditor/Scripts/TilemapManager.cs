/****************************************************
    File Name  : TilemapManager.cs
    Author     : MingJia Li
    Date & Ver : 2021/11/28 - Unity2021.1.19f1
    Description:
*****************************************************/

using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class TilemapManager
{
    public static string SourceDataPath => "Assets/TilemapEditor/MapsData/SourceData.asset";
    public static string MapsDataPath => "Assets/TilemapEditor/MapsData";
    public static string TilesFolderPath => "Assets/TilemapEditor/Tiles";

    private static TilemapSourceData _sourceData;
    private static List<TilemapData> _mapDataList;

    public static TilemapSourceData SourceData
    {
        get
        {
            if (_sourceData == null)
            {
                _sourceData = AssetDatabase.LoadAssetAtPath<TilemapSourceData>(SourceDataPath);
            }

            return _sourceData;
        }
    }

    private static List<CustomTile> _allTiles;

    public static List<CustomTile> AllTiles
    {
        get
        {
            if (_allTiles == null)
            {
                _allTiles = new List<CustomTile>();

                string[] tileFiles = Directory.GetFiles(Application.dataPath + "/TilemapEditor/Tiles", "*.asset", SearchOption.AllDirectories);
                foreach (string file in tileFiles)
                {
                    var assetPath = file;
                    assetPath = assetPath.Replace(Application.dataPath, "");
                    assetPath = "Assets" + assetPath;
                    assetPath = assetPath.Replace('/', '\\');
                    var tile = AssetDatabase.LoadAssetAtPath<CustomTile>(assetPath);
                    _allTiles.Add(tile);
                }
            }

            return _allTiles;
        }
    }

    public static void CreateSourceData()
    {
        _sourceData = ScriptableObject.CreateInstance<TilemapSourceData>();
        AssetDatabase.CreateAsset(_sourceData, SourceDataPath);

        if (_mapDataList == null)
        {
            _mapDataList = _sourceData.Data;
        }

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    public static TilemapData CreateMapData(string mapName)
    {
        if (_mapDataList == null)
        {
            _mapDataList = _sourceData.Data;
        }

        var tilemapData = ScriptableObject.CreateInstance<TilemapData>();
        mapName = string.IsNullOrEmpty(mapName) ? "New Tilemap" : mapName;
        var path = MapsDataPath + "/" + mapName + ".asset";
        if (System.IO.File.Exists(path))
        {
            return null;
        }

        AssetDatabase.CreateAsset(tilemapData, path);
        _mapDataList.Add(tilemapData);
        tilemapData.TilemapName = mapName;

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();

        return tilemapData;
    }

    public static void SaveMapData(TilemapData tilemapData)
    {
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    public static void ClearMapData()
    {
    }

    public static void DeleteMapData(string mapName)
    {
        if (_mapDataList == null)
        {
            return;
        }

        for (int i = 0; i < _mapDataList.Count; i++)
        {
            if (_mapDataList[i].TilemapName.Equals(mapName))
            {
                _mapDataList.RemoveAt(i);
                break;
            }
        }

        var path = MapsDataPath + "/" + mapName + ".asset";
        if (System.IO.File.Exists(path))
        {
            System.IO.File.Delete(path);
        }

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }
}