/****************************************************
    File Name  : TilemapSourceData.cs
    Author     : MingJia Li
    Date & Ver : 2021/11/27 - Unity2021.1.19f1
    Description:
*****************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu]
public class TilemapSourceData : ScriptableObject
{
    [SerializeField]
    public List<TilemapData> Data = new List<TilemapData>();
}

[CreateAssetMenu]
public class TilemapData : ScriptableObject
{
    public string TilemapName;

    [SerializeField]
    public List<TileInfo> TileInfos;
}

[Serializable]
public class TileInfo
{
    public Tile Tile;
    public Vector2 Position;
}