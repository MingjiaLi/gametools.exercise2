/****************************************************
    File Name  : TilemapWindow.cs
    Author     : MingJia Li
    Date & Ver : 2021/11/27 - Unity2021.1.19f1
    Description:
*****************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

public class TilemapWindow : EditorWindow
{
    private static TilemapWindow _window;
    private static Vector2 _windowSize = new Vector2(800, 600);
    private int _mapIndex;
    private int[] _intMapPopupSize;
    private string[] _stringMapPopupSize;
    private string _curTilemapName;
    private TilemapData _curTilemapData;

    private int _tileIndex;
    private int[] _intTilesPopupSize;
    private string[] _stringTilesPopupSize;
    private CustomTile _curCustomTile;

    [MenuItem("Tools/TileMap Editor %t")]
    public static void ShowTileMapEditor()
    {
        _window = (TilemapWindow)EditorWindow.GetWindow(typeof(TilemapWindow));
        _window.titleContent = new GUIContent("Tilemap Window");
        _window.minSize = _windowSize;
        _window.maxSize = _windowSize;
        _window.Show();
    }

    private void OnGUI()
    {
        EditorGUILayout.BeginVertical();

        PathConfigs();
        TitleMenu();
        MapView();

        EditorGUILayout.EndVertical();
    }

    private void Title(string title)
    {
        EditorGUILayout.Space();
        var labelStyle = new GUIStyle(EditorStyles.boldLabel);
        labelStyle.normal.textColor = Color.yellow;
        GUILayout.Label(title, labelStyle);
        GUI.backgroundColor = Color.white;
    }

    private void PathConfigs()
    {
        Title("Path Configs");
        EditorGUILayout.BeginVertical();

        GUILayout.Label("Source Data Path: " + TilemapManager.SourceDataPath, GUILayout.ExpandWidth(true));
        GUILayout.Label("Maps Data Path: " + TilemapManager.MapsDataPath, GUILayout.ExpandWidth(true));
        GUILayout.Label("Tiles Folder Path: " + TilemapManager.TilesFolderPath, GUILayout.ExpandWidth(true));

        EditorGUILayout.EndVertical();
    }

    private void TitleMenu()
    {
        Title("Map Menu");

        // 1 BeginHorizontal
        EditorGUILayout.BeginHorizontal();

        EditorGUILayout.Space();

        if (TilemapManager.SourceData != null)
        {
            // 2 BeginHorizontal
            EditorGUILayout.BeginHorizontal();

            int count = TilemapManager.SourceData.Data.Count;
            _intMapPopupSize = new int[count];
            _stringMapPopupSize = new string[count];
            for (int i = 0; i < count; i++)
            {
                _intMapPopupSize[i] = i;
                _stringMapPopupSize[i] = TilemapManager.SourceData.Data[i].TilemapName;
            }
            _mapIndex = EditorGUILayout.IntPopup("Selected Map: ", _mapIndex, _stringMapPopupSize, _intMapPopupSize, GUILayout.ExpandWidth(true));

            EditorGUILayout.Space();

            // 1 BeginVertical
            EditorGUILayout.BeginVertical();

            // 3 BeginHorizontal
            EditorGUILayout.BeginHorizontal();

            _curTilemapName = EditorGUILayout.TextField("New Tilemap Name: ", _curTilemapName);
            if (GUILayout.Button("Create Map Data"))
            {
                _curTilemapData = TilemapManager.CreateMapData(_curTilemapName);
                if (_curTilemapData == null)
                {
                    EditorUtility.DisplayDialog("Hints", $"Map name: {_curTilemapName} already exists!", "Okay");
                    return;
                }
                _curTilemapName = _curTilemapData.TilemapName;
                // _window.Repaint();
            }

            // 3 EndHorizontal
            EditorGUILayout.EndHorizontal();

            // 4 BeginHorizontal
            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("Save Map Data"))
            {
                var data = TilemapManager.SourceData.Data;
                if (data != null && _mapIndex < data.Count)
                {
                    _curTilemapData = data[_mapIndex];
                }

                if (_curTilemapData != null)
                {
                    if (EditorUtility.DisplayDialog("Hints", $"Save map named: {_curTilemapName}?", "Yes", "Cancel"))
                    {
                        TilemapManager.SaveMapData(_curTilemapData);
                    }
                }
            }

            if (GUILayout.Button("Clear Map Data"))
            {
                TilemapManager.ClearMapData();
            }

            if (GUILayout.Button("Delete Map Data"))
            {
                var data = TilemapManager.SourceData.Data;
                if (data != null && _mapIndex < data.Count)
                {
                    _curTilemapData = data[_mapIndex];
                }

                if (_curTilemapData != null)
                {
                    if (EditorUtility.DisplayDialog("Hints", $"Delete map named: {_curTilemapName}?", "Yes", "Cancel"))
                    {
                        TilemapManager.DeleteMapData(_curTilemapName);
                    }
                }
            }

            // 4 EndHorizontal
            EditorGUILayout.EndHorizontal();

            // 1 EndVertical
            EditorGUILayout.EndVertical();

            // 2 EndHorizontal
            EditorGUILayout.EndHorizontal();
        }
        else
        {
            if (GUILayout.Button("Create Source Data"))
            {
                TilemapManager.CreateSourceData();
                _window.Repaint();
            }
        }

        // 1 EndHorizontal
        EditorGUILayout.EndHorizontal();
    }

    private void MapView()
    {
        Title("Map View");

        EditorGUILayout.BeginVertical();

        var tiles = TilemapManager.AllTiles;
        var count = tiles.Count;
        _intTilesPopupSize = new int[count];
        _stringTilesPopupSize = new string[count];
        for (int i = 0; i < count; i++)
        {
            _intTilesPopupSize[i] = i;
            _stringTilesPopupSize[i] = tiles[i].name;
        }

        _tileIndex = EditorGUILayout.IntPopup("Selected Tile: ", _tileIndex, _stringTilesPopupSize, _intTilesPopupSize,
            GUILayout.ExpandWidth(true));

        for (int y = 0; y < 6; y++)
        {
            EditorGUILayout.BeginHorizontal();

            for (int x = 0; x < 11; x++)
            {
                GUILayout.Box("",
                    GUILayout.MinHeight(64),
                    GUILayout.MaxHeight(64),
                    GUILayout.MinWidth(64),
                    GUILayout.MaxWidth(64));
            }

            EditorGUILayout.EndHorizontal();
        }

        EditorGUILayout.EndVertical();

        if (_curTilemapData != null)
        {
            var tilesData = _curTilemapData.TileInfos;
        }
    }
}